﻿using Furion;
using Furion.Authorization; 
using Furion.DatabaseAccessor;
using Furion.DataEncryption;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using TulingMember.Core; 
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic; 
using System.Linq;
using System.Threading.Tasks;
using Yitter.IdGenerator;

namespace TulingMember.Application
{
    /// <summary>
    /// 角色管理服务
    /// </summary>
    
    public class RBACService : IDynamicApiController
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<UserRole> _userRoleRepository;
        private readonly IRepository<RoleSecurity> _roleSecurityRepository;
        private readonly IRepository<Security> _securityRepository;
        private readonly IRepository<IPLog> _iplogRepository;
        private readonly IAuthorizationManager _authorizationManager; 
        public RBACService(IHttpContextAccessor httpContextAccessor
            , IRepository<User> userRepository
            , IRepository<Role> roleRepository
            , IRepository<UserRole> userRoleRepository
            , IRepository<RoleSecurity> roleSecurityRepository
            , IRepository<Security> securityRepository
            , IRepository<IPLog> iplogRepository
            , IAuthorizationManager authorizationManager )
        {
            _httpContextAccessor = httpContextAccessor;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _userRoleRepository = userRoleRepository;
            _roleSecurityRepository = roleSecurityRepository;
            _securityRepository = securityRepository;
            _iplogRepository = iplogRepository;
            _authorizationManager = authorizationManager; 
        }

        /// <summary>
        /// 登录（免授权）
        /// </summary>
        /// <param name="input"></param>
        /// <remarks>管理员：admin/654321；</remarks>
        /// <returns></returns>
        [AllowAnonymous]
        public UserDto Login(LoginInput input)
        {
            // 验证用户名和密码
            var user = _userRepository.Where(u => u.Account.Equals(input.Account)&& !u.IsDeleted,false,true).FirstOrDefault() ?? throw Oops.Bah(ErrorCode.WrongUser).StatusCode(ErrorStatus.ValidationFaild);
            if (!user.Password.Equals(MD5Encryption.Encrypt(input.Password)))
            {
                throw Oops.Bah(ErrorCode.WrongPwd).StatusCode(ErrorStatus.ValidationFaild);
            }
            var output = user.Adapt<UserDto>();
            if (input.Password.Equals(AppStr.DeafultPwd))
            {
                output.InitPwd = 1;
            } 
            output.Token = JWTEncryption.Encrypt(new Dictionary<string, object>()
            {

                { ClaimConst.CLAINM_USERID, user.Id },
                { ClaimConst.CLAINM_ACCOUNT,user.Account },
                { ClaimConst.CLAINM_NAME, user.Name },
                { ClaimConst.USER_TYPE, user.UserType },
                { ClaimConst.TENANT_ID, user.TenantId },

            });
            
            //NetInfo netinfo = HttpHelper.getNetInfo();
            //var ipEntity = netinfo.Adapt<IPLog>();
            //ipEntity.UserId = user.Id;
            //ipEntity.UserAccount = user.Account;
            //ipEntity.UserName = user.Name;
            //ipEntity.TenantId = user.TenantId;
            //ipEntity.Id = YitIdHelper.NextId();
            //ipEntity.CreatedTime = DateTime.Now;
            //ipEntity.CreatedUserId= user.Id;
            //ipEntity.CreatedUserName = user.Name; 
            //_iplogRepository.Insert(ipEntity);
            // 设置 Swagger 刷新自动授权
            _httpContextAccessor.HttpContext.Response.Headers["access-token"] = output.Token;

            return output;
        }

        /// <summary>
        /// 查看用户信息
        /// </summary>
        public UserDto GetUserInfo()
        {
            // 获取用户Id
            var userId = _authorizationManager.GetUserId();
            var entity = _userRepository
                .Include(u => u.Roles)
                    .ThenInclude(u => u.Securities)
                .Where(u => u.Id == userId).FirstOrDefault(); 
          
            UserDto user = entity.Adapt<UserDto>();
            if (user.UserType == UserType.System)
            {
                user.Access = _securityRepository.AsQueryable().Select(m => m.UniqueCode).ToList();
            }
            else {
                user.Access = entity.Roles.SelectMany(r => r.Securities).Select(m => m.UniqueCode).ToList();
            }
            return user;
        }
        
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task UpdatePwd(UpdatePwdInput input)
        {
            var userId = _authorizationManager.GetUserId();
            var user = _userRepository.FindOrDefault(userId);
            
            if (!user.Password.Equals(MD5Encryption.Encrypt(input.OldPassword)))
            {
                throw Oops.Bah(ErrorCode.WrongOldPwd).StatusCode(ErrorStatus.ValidationFaild);
            }
            user.Password = MD5Encryption.Encrypt(input.Password); 
            return _userRepository.UpdateIncludeAsync(user,new string[] { "Password" });
        }
        /// <summary>
        /// 手机号+验证码重置密码
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public Task ResetPwd(ResetPwdInput input)
        {
            var oldUser = _userRepository.Where(m => m.Account == input.Account && !m.IsDeleted, false, true).FirstOrDefault();
            if (oldUser == null)
            {
                throw Oops.Bah(ErrorCode.WrongValidation, "该手机号未注册").StatusCode(ErrorStatus.ValidationFaild);
            }
            var codeKey = AppStr.ForgetPwdCodeKey + "_" + input.Account;
            var registCode = RedisHelper.GetString(codeKey);

            if (string.IsNullOrEmpty(registCode))
            {
                throw Oops.Bah("验证码超时或无效").StatusCode(ErrorStatus.ValidationFaild);
            }
            if (!registCode.Equals(input.Code))
            {
                throw Oops.Bah("验证码错误").StatusCode(ErrorStatus.ValidationFaild);
            } 
            oldUser.Password = MD5Encryption.Encrypt(input.Password);
            _userRepository.UpdateInclude(oldUser, new string[] { "Password" });
            RedisHelper.KeyDelete(codeKey);
            return Task.CompletedTask;
        }
        #region 权限管理
        /// <summary>
        /// 权限列表
        /// </summary>
        [SecurityDefine("auth")]
        public List<SecurityDto> GetAuth()
        {
            return _securityRepository.Where(m=>!m.IsDeleted).AsEnumerable().Adapt<List<SecurityDto>>();
        }
        /// <summary>
        /// 获取我拥有的权限
        /// </summary>
        /// <returns></returns>
        public List<SecurityDto> GetMyAuth()
        {
            var usertype = _authorizationManager.GetUserType();
            if (usertype == UserType.System)
            {
                return _securityRepository.Where(m => !m.IsDeleted).AsEnumerable().Adapt<List<SecurityDto>>();
            }
            else {
                var userId = _authorizationManager.GetUserId();
                var entity = _userRepository
                 .Include(u => u.Roles)
                     .ThenInclude(u => u.Securities)
                 .Where(u => u.Id == userId).FirstOrDefault();
                return entity.Roles.SelectMany(r => r.Securities).Adapt<List<SecurityDto>>();
            }           
        }
        /// <summary>
        /// 保存权限
        /// </summary>
        [SecurityDefine("auth")]
        public Task SaveAuth(SecurityDto input)
        {
            if (input.Id == 0)
            {
                var hasData= _securityRepository.Where(m => !m.IsDeleted && (m.UniqueCode == input.UniqueCode || m.UniqueName == input.UniqueName)).FirstOrDefault();
                if (hasData!=null)
                {
                    if (hasData.UniqueCode==input.UniqueCode)
                    {
                        throw Oops.Bah(ErrorCode.WrongValidation, "已存在此权限编码").StatusCode(ErrorStatus.ValidationFaild);
                    }
                    else if (hasData.UniqueName == input.UniqueName)
                    {
                        throw Oops.Bah(ErrorCode.WrongValidation, "已存在此权限名称").StatusCode(ErrorStatus.ValidationFaild);
                    }
                }
                return _securityRepository.InsertAsync(input.Adapt<Security>());
            }
            else {
                var hasData = _securityRepository.Where(m => !m.IsDeleted &&m.Id!=input.Id && (m.UniqueCode == input.UniqueCode || m.UniqueName == input.UniqueName)).FirstOrDefault();
                if (hasData != null)
                {
                    if (hasData.UniqueCode == input.UniqueCode)
                    {
                        throw Oops.Bah(ErrorCode.WrongValidation, "已存在此权限编码").StatusCode(ErrorStatus.ValidationFaild);
                    }
                    else if (hasData.UniqueName == input.UniqueName)
                    {
                        throw Oops.Bah(ErrorCode.WrongValidation, "已存在此权限名称").StatusCode(ErrorStatus.ValidationFaild);
                    }
                }
                return _securityRepository.UpdateAsync(input.Adapt<Security>());
            }
           
           
        }
        /// <summary>
        /// 删除权限
        /// </summary>
        [SecurityDefine("auth")]
        public Task DeleteAuth(long id)
        {
            return _securityRepository.FakeDeleteAsync(id);
        }
        #endregion

        #region 角色管理
        /// <summary>
        /// 角色列表
        /// </summary> 
        public List<RoleDto> GetRole()
        {
            return _roleRepository.AsEnumerable().Adapt<List<RoleDto>>();
        }


      
        /// <summary>
        /// 获取某个角色的权限
        /// </summary> 
        public List<SecurityDto> GetAuthByRoleId(long id)
        {
            var securities = _roleRepository.Include(u => u.Securities)
              .Where(u => u.Id == id).SelectMany(u => u.Securities); 
            return securities.Adapt<List<SecurityDto>>();
        }
        /// <summary>
        /// 保存角色
        /// </summary>
        [SecurityDefine("role")]
        public Task SaveRole(RoleDto input)
        {
            if (input.Id == 0)
            {

                var hasData = _roleRepository.Where(m => !m.IsDeleted && m.Name==input.Name).FirstOrDefault();
                if (hasData != null)
                {
                    throw Oops.Bah(ErrorCode.WrongValidation, "已存在此角色名称").StatusCode(ErrorStatus.ValidationFaild);
                }
                return _roleRepository.InsertAsync(input.Adapt<Role>());
            }
            else {
                var hasData = _roleRepository.Where(m => !m.IsDeleted && m.Name == input.Name&& m.Id!=input.Id).FirstOrDefault();
                if (hasData != null)
                {
                    throw Oops.Bah(ErrorCode.WrongValidation, "已存在此角色名称").StatusCode(ErrorStatus.ValidationFaild);
                }
                return _roleRepository.UpdateAsync(input.Adapt<Role>());
            }
           

        }
        /// <summary>
        /// 删除角色
        /// </summary>
        [SecurityDefine("role")]
        public Task DeleteRole(long id)
        {
           
            return _roleRepository.FakeDeleteAsync(id);
        }
        

        /// <summary>
        /// 为角色分配权限
        /// </summary>
        [SecurityDefine("role")]
        public Task GiveRoleSecurity(RoleSecurityInput input)
        {
            input.SecurityIds ??= Array.Empty<long>();
            _roleSecurityRepository.Delete(_roleSecurityRepository.Where(u => u.RoleId == input.RoleId).ToList());

            var list = new List<RoleSecurity>();
            foreach (var securityId in input.SecurityIds)
            {
                list.Add(new RoleSecurity { RoleId = input.RoleId, SecurityId = securityId });
            }

            return _roleSecurityRepository.InsertAsync(list);
        }
        #endregion

        #region 员工管理

        /// <summary>
        /// 查询员工
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [SecurityDefine("employee")] 
        public PagedList<UserDto> SearchEmployee(UserInput input)
        {
            var search = _userRepository.Where(m =>m.UserType!=UserType.System);
           
            if (!string.IsNullOrEmpty(input.keyword))
            {
                search = search.Where(m => m.Name.Contains(input.keyword) ||m.Account.Contains(input.keyword));
            }
            var users = search.ToPagedList(input.page, input.size).Adapt<PagedList<UserDto>>();
            return users;
        }
        
        /// <summary>
        /// 保存员工
        /// </summary>
        [SecurityDefine("employee")]
        public Task SaveEmployee(UserDto input)
        {
            var oldUser = _userRepository.Where(m => m.Account == input.Account && m.Id != input.Id).FirstOrDefault();
            if (oldUser != null)
            {
                throw Oops.Bah(ErrorCode.WrongValidation, "该账号已存在").StatusCode(ErrorStatus.ValidationFaild);
            }

            if (input.Id<=0)
            {
                
                User user = input.Adapt<User>(); 
                user.Password = MD5Encryption.Encrypt(AppStr.DeafultPwd);//默认密码为123456
                return _userRepository.InsertAsync(user);
            }
            else
            {
               
                User user = input.Adapt<User>();
                return _userRepository.UpdateExcludeAsync(user, new[] { nameof(user.IsDeleted), nameof(user.Password)});
            }
           

        }
        /// <summary>
        /// 删除员工
        /// </summary>
        [SecurityDefine("employee")]
        public Task DeleteEmployee(long id)
        {
            // 获取用户Id
            var userId = _authorizationManager.GetUserId();
            if (userId == id)
            {
                throw Oops.Bah(ErrorCode.WrongValidation, "不能删除当前账号").StatusCode(ErrorStatus.ValidationFaild);
            }
         
            return _userRepository.FakeDeleteAsync(id);
        }
        /// <summary>
        /// 获取某个员工的角色
        /// </summary>
        [SecurityDefine("employee")]
        public List<RoleDto> GetRoleByUserId(long id)
        {
            var roles = _userRepository.Include(u => u.Roles)
              .Where(u => u.Id == id).SelectMany(u => u.Roles);
            return roles.Adapt<List<RoleDto>>();
        }
        /// <summary>
        /// 为用户分配角色
        /// </summary>
        [SecurityDefine("employee")]
        public Task GiveUserRole(UserRoleInput input)
        {
            // 获取用户Id
             
            input.RoleIds ??= Array.Empty<long>();
            _userRoleRepository.Delete(_userRoleRepository.Where(u => u.UserId == input.UserId).ToList());

            var list = new List<UserRole>();
            foreach (var roleid in input.RoleIds)
            {
                list.Add(new UserRole { UserId = input.UserId, RoleId = roleid });
            }

            return _userRoleRepository.InsertAsync(list);
        }
        #endregion
    }
}