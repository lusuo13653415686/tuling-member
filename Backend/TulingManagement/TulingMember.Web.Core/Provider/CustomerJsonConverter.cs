﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace TulingMember.Web.Core.Provider
{
    public class IntJsonConverter : JsonConverter<int>
    {
        public override int Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            try
            {
                if (reader.TokenType == JsonTokenType.Number)
                {
                    return reader.GetInt32();
                }
                else if (reader.TokenType == JsonTokenType.Null)
                {
                    return 0;
                }
                else if (reader.TokenType == JsonTokenType.String)
                {
                    var rst = reader.GetString();
                    if (string.IsNullOrEmpty(rst))
                    {
                        return 0;
                    }
                    else
                    {
                        return int.Parse(rst);
                    }
                }
                return reader.GetInt32();
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public override void Write(Utf8JsonWriter writer, int value, JsonSerializerOptions options)
        {
            writer.WriteNumberValue(value);
        }
    }
    public class LongJsonConverter : JsonConverter<long>
    {
        public override long Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            try
            {
                if (reader.TokenType == JsonTokenType.Number)
                {
                    return reader.GetInt64();
                }
                else if (reader.TokenType == JsonTokenType.Null)
                {
                    return 0;
                }
                else if (reader.TokenType == JsonTokenType.String)
                {
                    var rst = reader.GetString();
                    if (string.IsNullOrEmpty(rst))
                    {
                        return 0;
                    }
                    else
                    {
                        return long.Parse(rst);
                    }
                }
                return reader.GetInt64();
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public override void Write(Utf8JsonWriter writer, long value, JsonSerializerOptions options)
        {
            writer.WriteNumberValue(value);
        }
    }
    public class DecimalJsonConverter : JsonConverter<decimal>
    {
        public override decimal Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            try
            {
                if (reader.TokenType == JsonTokenType.Number)
                {
                    return reader.GetDecimal();
                }
                else if (reader.TokenType == JsonTokenType.Null)
                {
                    return 0;
                }
                else if (reader.TokenType == JsonTokenType.String)
                {
                    var rst = reader.GetString();
                    if (string.IsNullOrEmpty(rst))
                    {
                        return 0;
                    }
                    else
                    {
                        return decimal.Parse(rst);
                    }
                }
                return reader.GetDecimal();
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public override void Write(Utf8JsonWriter writer, decimal value, JsonSerializerOptions options)
        {
            writer.WriteNumberValue(value);
        }
    }
    /// <summary>
    /// Json任何类型读取到字符串属性
    /// 因为 System.Text.Json 必须严格遵守类型一致，当非字符串读取到字符属性时报错：
    /// The JSON value could not be converted to System.String.
    /// </summary>
    public class StringJsonConverter : JsonConverter<string>
    {
        public override string Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {

            if (reader.TokenType == JsonTokenType.String)
            {
                return reader.GetString();
            }
            else
            {//非字符类型，返回原生内容
                return GetRawPropertyValue(reader);
            }

            throw new JsonException();
        }

        public override void Write(Utf8JsonWriter writer, string value, JsonSerializerOptions options)
        {
            writer.WriteStringValue(value);
        }
        /// <summary>
        /// 非字符类型，返回原生内容
        /// </summary>
        /// <param name="jsonReader"></param>
        /// <returns></returns>
        private static string GetRawPropertyValue(Utf8JsonReader jsonReader)
        {
            ReadOnlySpan<byte> utf8Bytes = jsonReader.HasValueSequence ?
            jsonReader.ValueSequence.ToArray() :
            jsonReader.ValueSpan;
            return Encoding.UTF8.GetString(utf8Bytes);
        }
    }
}
