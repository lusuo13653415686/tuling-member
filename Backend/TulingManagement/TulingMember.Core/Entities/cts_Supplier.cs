﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace TulingMember.Core
{
    /// <summary>
    /// 。
    /// </summary> 
    [TableAudit]
    public class cts_Supplier: DEntityTenant
    {
     
        /// <summary>
        /// 。
        /// </summary>
     
        public string Name { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public string Phone { get; set; }

    
        /// <summary>
        /// 余额。
        /// </summary>
     
        public decimal Balance { get; set; }

        /// <summary>
        /// 。
        /// </summary>

        public string Address { get; set; }
        /// <summary>
        /// 。
        /// </summary>

        public string Remark { get; set; }


    }
}