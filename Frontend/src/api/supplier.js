import { axios } from '@/libs/api.request'

// #region  客户
export const SearchSupplier = form => {
  return axios.post('api/Supplier/SearchSupplier', form)
}

export const SaveSupplier = form => {
  return axios.post('api/Supplier/SaveSupplier', form)
}

export const DeleteSupplier = id => {
  return axios.post('api/Supplier/DeleteSupplier/' + id)
}

export const GetSupplier = id => {
  return axios.get('api/Supplier/GetSupplier/' + id)
}
export const SearchSupplierPayLog = form => {
  return axios.post('api/Supplier/SearchSupplierPayLog', form)
}
export const AddSupplierPayLog = form => {
  return axios.post('api/Supplier/AddSupplierPayLog', form)
}

// #endregion
