import { axios } from '@/libs/api.request'

// #region  销售单
export const SearchSaleOrder = form => {
  return axios.post('api/SaleOrder/SearchSaleOrder', form)
}

export const SaveSaleOrder = form => {
  return axios.post('api/SaleOrder/SaveSaleOrder', form)
}
export const GetSaleOrder = id => {
  return axios.get('api/SaleOrder/GetSaleOrder/' + id)
}
export const GetSaleOrderAndPrint = id => {
  return axios.get('api/SaleOrder/GetSaleOrderAndPrint/' + id)
}

export const DeleteSaleOrder = id => {
  return axios.post('api/SaleOrder/DeleteSaleOrder/' + id)
}
export const CheckSaleOrder = form => {
  return axios.post('api/SaleOrder/CheckSaleOrder', form)
}
